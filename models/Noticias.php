<?php
namespace app\models;

use yii\base\Model;


class Noticias extends Model{
    public string $title;
    public string $link;
    public string $description;
    
    public function attributeLabels() {
        return [
            "title" => "Titulo",
            "link" => "Enlace",
            "description" => "Descripcion"
        ];
    }
    
    public function rules(): array {
        return [
            [['title','description','link'],'safe']
        ];
    }
    
    public function crearEnlace(){
        return \yii\helpers\Html::a(
                "Ver Noticia",
                $this->link,
                ["class" => "btn btn-primary"]
        );
    }
    
    public function crearImagen(){
        return str_replace(
                "src", 
                'class="img-thumbnail" src', 
                $this->description
                );
    }
    
    public function getEnlace(){
        return \yii\helpers\Html::a(
                "Ver Noticia",
                $this->link,
                ["class" => "btn btn-primary"]
        );
    }
    
    public function getDescripcion(){
        return str_replace(
                "src", 
                'class="img-thumbnail" src', 
                $this->description
                );
    }
    
    
}
