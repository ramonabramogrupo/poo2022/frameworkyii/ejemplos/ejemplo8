<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
     
       $vectorNoticias= $this->leerNoticias("https://www.eldiariomontanes.es/rss/2.0/?section=ultima-hora");
        
        // noticias es una array con todas noticias
        // mostreis todas las noticias en un GRIDVIEW
        
        // creo un dataProvider con arrayDataProvider
        $dataProvider=new \yii\data\ArrayDataProvider([
            "allModels" => $vectorNoticias,
            'pagination' => [
                'pageSize' => 10
            ],
        ]);
        
        return $this->render("index",[
            "dataProvider" => $dataProvider,
            "titulo" => "Noticias ultima hora"
        ]);   
    }

     public function actionCine()
    {
        $vectorNoticias= $this->leerNoticias("https://www.eldiariomontanes.es/rss/2.0/?section=butaca/cine");
        
        // noticias es una array con todas noticias
        // mostreis todas las noticias en un GRIDVIEW
        
        // creo un dataProvider con arrayDataProvider
        $dataProvider=new \yii\data\ArrayDataProvider([
            "allModels" => $vectorNoticias,
            'pagination' => [
                'pageSize' => 10
            ],
        ]);
        
        return $this->render("index",[
            "dataProvider" => $dataProvider,
            "titulo" => "Noticias de Cine"
        ]);   
    }
    
     public function actionRecetas()
    {
        $vectorNoticias= $this->leerNoticias("https://www.eldiariomontanes.es/rss/2.0/?section=cantabria-mesa/recetas");
        
        // noticias es una array con todas noticias
        // mostreis todas las noticias en un GRIDVIEW
        
        // creo un dataProvider con arrayDataProvider
        $dataProvider=new \yii\data\ArrayDataProvider([
            "allModels" => $vectorNoticias,
            'pagination' => [
                'pageSize' => 10
            ],
        ]);
        
        return $this->render("index",[
            "dataProvider" => $dataProvider,
            "titulo" => "Recetas"
        ]);   
    }
    
    /**
     * Metodo que devuelve un array de Objetos 
     * de tipo Noticia
     * @param type $url Es la direccion del servidor RSS
     * @return \app\models\Noticias Array de Noticias(modelos)
     */
    private function leerNoticias($url){
        
        // leer una pagina web
        $contenido=file_get_contents($url);
        
        // funcion de php lea xml y lo convierta a array
        $noticias=simplexml_load_string($contenido)->channel->item;
        
        // crear un array con todas las noticias
        foreach ($noticias as $noticia){
            // noticia
            // es ub objeto de tipo XMLElement
            
            // objetoMioNoticia es un modelo 
            // es un objeto creado a partir de la clase Noticias
            $objetoMioNoticia=new \app\models\Noticias();
            
            // sin utilizar asignacion masiva
            //$objetoMioNoticia->title=$noticia->title;
            //$objetoMioNoticia->link=$noticia->link;
            //$objetoMioNoticia->description=$noticia->description;
            
            // con asignacion masiva
            $objetoMioNoticia->attributes=(array)$noticia;
            
            
            
            // me voy creando un array con objetos 
            // de tipo Noticias (modelos)
            $vectorNoticias[]=$objetoMioNoticia;
        }
        
        return $vectorNoticias;
        
    }
}
