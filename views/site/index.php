<?php

/* @var $this yii\web\View */

$this->title = $titulo;
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4"><?= $this->title ?></h1>
    </div>
    <?= 
        yii\grid\GridView::widget([
            "dataProvider" => $dataProvider,
            "columns" =>[
              
             "title", // colocando directamente una propiedad
              // colocar un metodo del modelo
              /*[
                "label" => "Descripcion",
                "content" => function($model){
                    return $model->crearImagen();
                }
              ],*/
              
              // colocar un metodo del modelo
              /*[
                  "label" => "Enlace",
                  "content" => function($model){
                    return $model->crearEnlace();
                  }
              ],*/
                      
              // colocar un getter del modelo
              [
                  "attribute" => "enlace",
                  "format" => "raw"
              ],
                      
              // colocar un getter del modelo
              [
                  "attribute" => "descripcion",
                  "format" => "raw"
              ],
            ],
        ]);
    ?>
  
</div>
